package polymorphism.inheritance;

public class ElectronicBook extends Book{
    private int numberBytes;

    public int getNumberOfBytes(){
        return this.numberBytes;
    }
    public ElectronicBook(String title, String author, int numberBytes){
        super(title,author);
        this.numberBytes=numberBytes;
    }

    // @Override
    public String toString(){
        // return "Title: "+getTitle()+", Author: "+getAuthor()+", Number of bytes: "+ this.numberBytes;
        return super.toString()+", Number of bytes: "+ this.numberBytes;
    }
}
