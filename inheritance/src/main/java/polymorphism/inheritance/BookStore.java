package polymorphism.inheritance;

public class BookStore {
    public static void main(String[] args){
        Book[] bookList = new Book[5];
        bookList[0] = new Book("Tome 1","Guy 1");
        bookList[1] = new ElectronicBook("ETome 2", "Guy 2", 133);
        bookList[2] = new Book("Tome 3","Guy 3");
        bookList[3] = new ElectronicBook("ETome 4", "Guy 4", 236);
        bookList[4] = new ElectronicBook("ETome 5", "Guy 5", 175);

        for(int i=0; i < bookList.length; i++){
            System.out.println(bookList[i]);
        }
    }
}
